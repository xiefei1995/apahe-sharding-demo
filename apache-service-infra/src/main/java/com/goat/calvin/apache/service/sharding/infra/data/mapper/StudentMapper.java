package com.goat.calvin.apache.service.sharding.infra.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.goat.calvin.apache.service.sharding.infra.model.po.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}