package com.goat.calvin.apache.service.sharding.infra.model.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p> 用户信息res <p/>
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/21 11:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoRes {
    /** token是否有效 */
    private Boolean valid;
    /** 登录名 */
    private String loginName;
    /** 真名 */
    private String realName;
    /** userId */
    private Long userId;
    /** .....others....*/
}
