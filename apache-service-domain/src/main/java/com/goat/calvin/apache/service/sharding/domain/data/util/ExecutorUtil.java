package com.goat.calvin.apache.service.sharding.domain.data.util;

import org.springframework.stereotype.Component;

import java.util.concurrent.*;

/**
 * <p> 线程池 <p/>
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/14 17:02
 */
@Component
public class ExecutorUtil {

    /** 核心线程数 */
    private final int corePoolSize = 8;
    /** 最大线程数 */
    private final int maxPoolSize = 16;
    /** 线程池存活时间 */
    private final long keepAliveTime = 60L;
    /** 时间单位 */
    private final TimeUnit timeUnit = TimeUnit.SECONDS;
    /** 阻塞队列 */
    private final BlockingQueue<Runnable> blockingQueue = new LinkedBlockingDeque<>();

    public static ThreadPoolExecutor execute(int corePoolSize,int maxPoolSize,long keepAliveTime,TimeUnit timeUnit,BlockingQueue<Runnable> blockingQueue){
        return new ThreadPoolExecutor(corePoolSize,maxPoolSize,keepAliveTime,timeUnit,blockingQueue,
                Executors.defaultThreadFactory(),new ThreadPoolExecutor.AbortPolicy());

    }
}
