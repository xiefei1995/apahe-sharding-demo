//package com.goat.calvin.apache.service.sharding.domain.data.util;
//
//import com.goat.calvin.apache.service.sharding.domain.config.RabbitMqConfig;
//import com.rabbitmq.client.Channel;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.messaging.handler.annotation.Payload;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//
///**
// * <p> 消息队列 util <p/>
// * @author xiefei15
// * @version 1.0.0
// * @since 2020/9/16 16:08
// */
//@Component
//@Slf4j
//public class RabbitMqUtil {
//
//    private final static String OBJECTID = "v01";
//
//    @Resource
//    private RabbitTemplate rabbitTemplate;
//
//    /**
//     * @param data  数据
//     * @param expiration 延时时间(单位：毫秒)
//     */
//    public void sendDelayMessage(Object data,String expiration){
//        log.info("---------delay message send start---------");
//        try{
//            CorrelationData correlationData = new CorrelationData(OBJECTID);
//            rabbitTemplate.convertAndSend(RabbitMqConfig.BASE_EXCHANGE,RabbitMqConfig.BASE_ROUTINGKEY,data, message ->{
//                message.getMessageProperties().setExpiration(expiration);
//                return message;
//            },correlationData);
//            log.info("----------delay message send success----------");
//        }catch (Exception e){
//            log.error("error info :" + e);
//        }
//    }
//
//    /**
//     * topic 服务
//     * @param data  数据
//     */
//    public void sendTopicMessage(Object data,String expiration){
//        log.info("---------topic message send start---------");
//        try{
//            CorrelationData correlationData = new CorrelationData(OBJECTID);
//            rabbitTemplate.convertAndSend(RabbitMqConfig.BASE_TOPIC_EXCHANGE,RabbitMqConfig.BASE_TOPIC_ROUTINGKEY,data,correlationData);
//            log.info("----------topic message send success----------");
//        }catch (Exception e){
//            log.error("error info :" + e);
//        }
//    }
//
//
//    /**
//     * @param data 数据
//     */
//    public void sendMessage(Object data){
//        log.info("---------message send start---------");
//        try{
//            CorrelationData correlationData = new CorrelationData(OBJECTID);
//            rabbitTemplate.convertAndSend(RabbitMqConfig.BASE_EXCHANGE,RabbitMqConfig.BASE_ROUTINGKEY,data,correlationData);
//        }catch (Exception e){
//            log.error("error info :" + e);
//        }
//    }
//
//
//    @RabbitListener(queues = RabbitMqConfig.BASE_QUEUE)
//    @RabbitHandler
//    public void messageListener(@Payload Object data, Message message, Channel channel) throws IOException {
//        /** do with data */
//        try{
//            System.out.println(data);
//        }catch (Exception e){
//            log.error("error:" + e);
//        }
//        /** 确认消费 */
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//    }
//
//
//    @RabbitListener(queues = RabbitMqConfig.BASE_DELAY_QUEUE)
//    @RabbitHandler
//    public void delayMessageListener(@Payload Object data, Message message, Channel channel) throws IOException {
//        /** do with data */
//        try{
//            System.out.println(data);
//        }catch (Exception e){
//            log.error("error:" + e);
//        }
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//    }
//
//}
