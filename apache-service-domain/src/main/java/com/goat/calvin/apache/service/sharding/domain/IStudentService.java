package com.goat.calvin.apache.service.sharding.domain;

import com.baomidou.mybatisplus.extension.service.IService;
import com.goat.calvin.apache.service.sharding.infra.model.po.Student;

/**
 * <p> 学生service <p/>
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/21 16:13
 */
public interface IStudentService extends IService<Student>{

    void addStudent(Student student);
}
