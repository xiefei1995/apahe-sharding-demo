package com.goat.calvin.apache.service.sharding.domain.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.goat.calvin.apache.service.sharding.domain.IStudentService;
import com.goat.calvin.apache.service.sharding.infra.data.mapper.StudentMapper;
import com.goat.calvin.apache.service.sharding.infra.model.po.Student;
import org.springframework.stereotype.Service;

/**
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/21 16:20
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {
    @Override
    public void addStudent(Student student) {
        this.save(student);
    }
}
