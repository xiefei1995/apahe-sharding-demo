package com.goat.calvin.apache.service.sharding.api.controller;

import com.goat.calvin.apache.service.sharding.api.controller.base.Result;
import com.goat.calvin.apache.service.sharding.domain.IStudentService;
import com.goat.calvin.apache.service.sharding.infra.model.po.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/21 16:23
 */
@RestController("/demo/student")
public class StudentController extends BaseController{

    @Autowired
    private IStudentService studentService;

    @GetMapping("/save")
    public Result<Void> addStudent(){
        Student student = new Student(null,"num01","张飞");
        studentService.addStudent(student);
        return Result.successResult("插入成功！");
    }
}
