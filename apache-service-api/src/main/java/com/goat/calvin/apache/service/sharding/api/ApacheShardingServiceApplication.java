package com.goat.calvin.apache.service.sharding.api;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


/**
 * @author xiefei15
 */
@SpringBootApplication
@EnableEurekaClient
@EnableApolloConfig
@MapperScan("com.goat.calvin.apache.service.sharding.infra.data.mapper")
public class ApacheShardingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApacheShardingServiceApplication.class, args);
    }

}
