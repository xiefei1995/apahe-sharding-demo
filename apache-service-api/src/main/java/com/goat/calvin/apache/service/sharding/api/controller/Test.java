package com.goat.calvin.apache.service.sharding.api.controller;

import java.util.*;

public class Test {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        List<Integer> list1 = new ArrayList<>();
        for(int i = 0;i < count;i++){
            Scanner sca1 = new Scanner(System.in);
            int x = sca1.nextInt();
            if(!list1.contains(x)){
                list1.add(x);
            }
        }

        Scanner scanner2 = new Scanner(System.in);
        int count2 = scanner2.nextInt();
        List<Integer> list2 = new ArrayList<>();
        for(int j = 0;j < count2;j++){
            Scanner sca1 = new Scanner(System.in);
            int x = sca1.nextInt();
            if(!list2.contains(x)){
                list2.add(x);
            }
        }
        list1.sort(Comparator.naturalOrder());
        list2.sort(Comparator.naturalOrder());


        list1.forEach(System.out::println);

        list2.forEach(System.out::println);
    }
}
