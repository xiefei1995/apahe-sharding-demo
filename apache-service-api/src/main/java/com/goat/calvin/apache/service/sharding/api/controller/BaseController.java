package com.goat.calvin.apache.service.sharding.api.controller;

import com.goat.calvin.apache.service.sharding.api.controller.base.ControllerBase;
import com.goat.calvin.apache.service.sharding.api.controller.base.Result;
import com.goat.calvin.apache.service.sharding.infra.model.res.UserInfoRes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * <p> 父类controller <p/>
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/21 14:54
 */
public class BaseController extends ControllerBase {

    /**
     * 获取当前请求的相关信息
     * @return ServletRequestAttributes
     */
    public HttpServletRequest getHttpServletRequest() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        return sra.getRequest();
    }

    /** 解析token */
    public Result<UserInfoRes> analysisToken(String token, String appId){
        /**
         *  解析token
         */
        UserInfoRes userInfoRes = new UserInfoRes(true,"guest","guest",1302818420137586689L);
        return Result.successResult(userInfoRes);
    }
}
