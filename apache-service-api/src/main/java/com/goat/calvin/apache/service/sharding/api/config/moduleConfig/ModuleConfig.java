package com.goat.calvin.apache.service.sharding.api.config.moduleConfig;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p> module在被web层引用时加入配置文件自定扫描这里定义的包 <p/>
 * @author xiefei15
 * @version 1.0.0
 * @since 2020/9/22 16:55
 */
@ComponentScan(basePackages = {
        "com.goat.calvin.apache.service"
})
@EnableFeignClients(
        basePackages = {
                "com.goat.calvin.apache.service.sharding.infra.data.feign"
        }
)
public class ModuleConfig {
    public ModuleConfig(){

    }
}
