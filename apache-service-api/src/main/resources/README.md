#  application.yml为主配置文件 其余配置写到 Apollo
#  web端配置
server.hostname = localhost
server.port = 10301
server.servlet.context-path = /web
spring.application.name = hdsp-csup-web-app
serverAppId = 1300734934630318081

# service层配置

server.port = 10334
server.servlet.context-path = /supervise
spring.application.name = hdsp-service-supervise
sharding.jdbc.datasource.names = hdsp
sharding.jdbc.datasource.hdsp.type = com.zaxxer.hikari.HikariDataSource
sharding.jdbc.datasource.hdsp.driver-class-name = com.mysql.cj.jdbc.Driver
sharding.jdbc.datasource.hdsp.jdbc-url = jdbc:mysql://10.197.236.152:3306/hdsp?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8&useAffectedRows=true&useSSL=false
sharding.jdbc.datasource.hdsp.username = root
sharding.jdbc.datasource.hdsp.password = 123456
sharding.jdbc.config.sharding.props.sql.show = true
sharding.jdbc.config.sharding.tables.supervise_process_info.actual-data-nodes = hdsp.supervise_process_info_$->{202004..202010}
sharding.jdbc.config.sharding.tables.supervise_process_info.table-strategy.standard.sharding-column = generated_time
sharding.jdbc.config.sharding.tables.supervise_process_info.table-strategy.standard.precise-algorithm-class-name = com.hikcreate.hdsp.service.supervise.infra.config.PreciseShardingRule
sharding.jdbc.config.sharding.tables.supervise_process_info.table-strategy.standard.range-algorithm-class-name = com.hikcreate.hdsp.service.supervise.infra.config.RangeShardingRule
mybatis-plus.mapper-locations = classpath*:mapper/*.xml
